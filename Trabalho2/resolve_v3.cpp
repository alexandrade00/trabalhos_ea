#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

void addNode(int *tree, int parent, int child);
void dfs(int *tree, int *dp, int src, int par, int *cost, int N);
void minSizeVertexCover(int *tree, int N, int *paidAmount);

int main() {

  string token;
  int *nodes = new int[1000000];
  int paidAmount[100000];
  int parentNode = 0;
  int input[11];

  fill(nodes, nodes + 1000000, -1);
  fill(paidAmount, paidAmount + 100000, -1);
  fill(input, input + 11, 0);

  while (cin >> parentNode) {

    // cout << parentNode << " ";
    if (parentNode == -1) {
      minSizeVertexCover(nodes, 100000, paidAmount);
      fill(nodes, nodes + 1000000, -1);
      fill(paidAmount, paidAmount + 100000, -1);
      fill(input, input + 11, 0);
    } else {
      int i = 0;
      while (cin.peek() != '\n') {
        cin >> input[i++];
        //cout << input[i] << " ";
      }
      // cout << endl;

      for (int j = 0; j < i - 1; j++)
        addNode(nodes, parentNode, input[j]);

      paidAmount[parentNode] = input[i - 1];
    }
  }

  delete[] nodes;
  return 0;
}

void addNode(int *tree, int parent, int child) {

  for (int i = 0; i < 10; i++) {
    if (tree[parent * 10 + i] == -1) {
      tree[parent * 10 + i] = child;
      break;
    }
  }

  for (int i = 0; i < 10; i++) {
    if (tree[child * 10 + i] == -1) {
      tree[child * 10 + i] = parent;
      break;
    }
  }
}

void dfs(int *tree, int *dp, int src, int par, int *cost, int N) {

  for (int i = 0; i < 10; i++) {
    int child = tree[src * 10 + i];
    if (child != par && child != -1)
      dfs(tree, dp, child, src, cost, N);
  }

  for (int i = 0; i < 10; i++) {
    int child = tree[src * 10 + i];
    if (child != par && child != -1) {
      // not including source in the vertex cover
      dp[src * N + 0] += dp[child * N + 1];
      cost[src * N + 0] += cost[child * N + 1];

      // including source in the vertex cover
      if (dp[child * N + 1] < dp[child * N + 0]) {
        dp[src * N + 1] += dp[child * N + 1];
        cost[src * N + 1] += cost[child * N + 1];
      } else if (dp[child * N + 1] > dp[child * N + 0]) {
        dp[src * N + 1] += dp[child * N + 0];
        cost[src * N + 1] += cost[child * N + 0];
      } else {
        if (cost[child * N + 1] > cost[child * N + 0]) {
          dp[src * N + 1] += dp[child * N + 1];
          cost[src * N + 1] += cost[child * N + 1];
        } else if (cost[child * N + 1] < cost[child * N + 0]) {
          dp[src * N + 1] += dp[child * N + 0];
          cost[src * N + 1] += cost[child * N + 0];
        } else {
          dp[src * N + 1] += dp[child * N + 1];
          cost[src * N + 1] += cost[child * N + 1];
        }
      }
      // dp[src][1] += min(dp[child][1], dp[child][0]);
    }
  }
}

void minSizeVertexCover(int *adj, int N, int *paidAmount) {
  // vector<int> dp[N];
  // vector<int> cost[N];
  int *dp = new int[N * 2];
  int *cost = new int[N * 2];

  fill(dp, dp + N * 2, 0);
  fill(cost, cost + N * 2, 0);

  for (int i = 0; i < N; i++) {
    // 1 denotes included in vertex cover
    dp[i * 2 + 1] = 1;
    cost[i * 2 + 1] = paidAmount[i];
  }

  dfs(adj, dp, 0, -1, cost, 2);

  ios::sync_with_stdio(true);
  // printing minimum size vertex cover
  if (dp[0 * 2 + 0] < dp[0 * 2 + 1]) {
    cout << dp[0 * 2 + 0] << " " << cost[0 * 2 + 0] << endl;
  } else if (dp[0 * 2 + 0] > dp[0 * 2 + 1]) {
    cout << dp[0 * 2 + 1] << " " << cost[0 * 2 + 1] << endl;
  } else {
    if (cost[0 * 2 + 0] > cost[0 * 2 + 1]) {
      cout << dp[0 * 2 + 0] << " " << cost[0 * 2 + 0] << endl;
    } else if (cost[0 * 2 + 0] < cost[0 * 2 + 1]) {
      cout << dp[0 * 2 + 1] << " " << cost[0 * 2 + 1] << endl;
    } else {
      cout << dp[0 * 2 + 1] << " " << cost[0 * 2 + 1] << endl;
    }
  }

  delete[] dp;
  delete[] cost;
  // cout << min(dp[0][0], dp[0][1]) << " " << endl;
}