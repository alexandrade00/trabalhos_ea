#include <algorithm>
#include <cstddef>
#include <iostream>
#include <string>

using namespace std;

void printArray(bool *array, int tam_rows_cols);
void printArray(short *array, int tam_rows_cols);
long searchNodeIndex(long *nodeArray, long arraySize, long elem);

int main() {
  istreambuf_iterator<char> begin(cin), end;
  string s(begin, end);

  string testcases_delimiter = "-1";
  string newline_delimiter = "\n";

  size_t pos_all = 0;
  size_t pos_one_test_case = 0;

  // delimit inputs by test cases
  string token;
  while ((pos_all = s.find(testcases_delimiter)) != string::npos) {

    long num_nodes = 0;
    token = s.substr(0, pos_all);
    s.erase(0, pos_all + testcases_delimiter.length() + 1);

    // Count newlines, each newline equals to one node
    string aux_string;
    string token_copy = token;
    while ((pos_one_test_case = token_copy.find(newline_delimiter)) !=
           string::npos) {
      aux_string = token_copy.substr(0, pos_one_test_case);
      token_copy.erase(0, pos_one_test_case + newline_delimiter.length());
      num_nodes++;
    }

    // save nodes id
    long nodes_id[num_nodes];
    long current_node = 0;
    token_copy = token;
    while ((pos_one_test_case = token_copy.find(newline_delimiter)) !=
           string::npos) {
      aux_string = token_copy.substr(0, pos_one_test_case + 1);

      // read each line information and populate data structs
      string concat_nums = "";
      for (long unsigned int i = 0; i < aux_string.length(); i++) {
        if (aux_string.at(i) == ' ') {
          nodes_id[current_node] = stol(concat_nums);
          break;
        } else {
          concat_nums += aux_string.at(i);
        }
      }

      token_copy.erase(0, pos_one_test_case + newline_delimiter.length());
      current_node++;
    }

    // initialize cache and fill with false's
    bool dp_table[num_nodes * num_nodes];
    fill(dp_table, dp_table + num_nodes * num_nodes, false);

    // array with amount paid by each node
    long paidAmount[num_nodes];
    fill(paidAmount, paidAmount + num_nodes, 0);

    // parse thourgh each line
    current_node = 0;
    while ((pos_one_test_case = token.find(newline_delimiter)) !=
           string::npos) {
      aux_string = token.substr(0, pos_one_test_case + 1);

      // read each line information and populate data structs
      string concat_nums = "";
      for (long unsigned int i = 0; i < aux_string.length(); i++) {
        if (aux_string.at(i) == ' ') {
          if (nodes_id[current_node] != stol(concat_nums)) {
            long node_index =
                searchNodeIndex(nodes_id, num_nodes, stol(concat_nums));
            dp_table[current_node * num_nodes + node_index] = true;
            // dp_table[stol(concat_nums) * num_nodes + current_node] = true;
          }
          concat_nums = "";
        } else if (aux_string.at(i) == '\n') {
          paidAmount[current_node] = stol(concat_nums);
        } else {
          concat_nums += aux_string.at(i);
        }
      }

      token.erase(0, pos_one_test_case + newline_delimiter.length());
      current_node++;
    }

    int parent_nodes[num_nodes];
    parent_nodes[0] = -1;

    for (int i = 1; i < num_nodes; i++) {
      for (int j = 0; j < num_nodes; j++) {
        if (dp_table[j * num_nodes + i] == true) {
          parent_nodes[i] = j;
          break;
        }
      }
    }

    // cycle to iterate leaf nodes
    bool is_leaf;
    bool is_visited[num_nodes];
    int investigatedMembers;
    int thisNode;
    int parentNode;
    int minInvestigatedMembers = 1000000;
    int maxCost = 0;
    int cost = 0;
    // at beginning all nodes are not visited
    fill(is_visited, is_visited + num_nodes, false);
    is_visited[0] = true;
    investigatedMembers = 0;
    cost = 0;

    for (int i = num_nodes - 1; i >= 0; i--) {

      // check if node is leaf
      is_leaf = true;
      for (long nodes = 0; nodes < num_nodes; nodes++) {
        if (dp_table[i * num_nodes + nodes] == true) {
          is_leaf = false;
          break;
        }
      }

      // go to next node if it is not leaf
      if (is_leaf != true || is_visited[i] == true)
        continue;

      // starter bottom-up node
      thisNode = parent_nodes[i];
      // parentNode = parent_nodes[thisNode];
      // int parentNodeCost = paidAmount[parent_nodes[parentNode]];
      // for (long nodes = 0; nodes < num_nodes; nodes++) {
      //   if (dp_table[parentNode * num_nodes + nodes] == true) {
      //     parentNodeCost += paidAmount[nodes];
      //   }
      // }

      // if (parentNodeCost > paidAmount[parentNode]) {
      //   thisNode = i;
      // } else {
      //   thisNode = parentNode;
      // }

      // climb tree up to root node
      while (true) {

        if (thisNode == 0) {
          int num_of_unvisited_childs = 0;
          int last_node = -1;
          for (long nodes = 0; nodes < num_nodes; nodes++) {
            if (dp_table[thisNode * num_nodes + nodes] == true &&
                is_visited[nodes] == false) {
              last_node = nodes;
              num_of_unvisited_childs++;
            }
          }
          if (num_of_unvisited_childs == 1 &&
              paidAmount[last_node] > paidAmount[thisNode]) {
            cost += paidAmount[last_node];
            is_visited[last_node] = true;
            is_visited[thisNode] = true;
            investigatedMembers++;
          } else if (num_of_unvisited_childs > 1) {
            cost += paidAmount[thisNode];
            is_visited[thisNode] = true;
            investigatedMembers++;
            for (long nodes = 0; nodes < num_nodes; nodes++) {
              if (dp_table[thisNode * num_nodes + nodes] == true &&
                  is_visited[nodes] == false) {
                last_node = nodes;
                is_visited[nodes] = true;
              }
            }
          } else if (!is_visited[thisNode]) {
            cost += paidAmount[thisNode];
            is_visited[thisNode] = true;
            investigatedMembers++;
          }
          break;
        } else {

          // search for parent of current node
          parentNode = parent_nodes[thisNode];

          // count investigated nodes and sum cost, leaf node and root does not
          bool is_investigated = false;
          bool parent_already_visited = false;
          if (is_visited[thisNode] == false) {
            investigatedMembers++;
            is_visited[thisNode] = true;
            is_investigated = true;
            cost += paidAmount[thisNode];
            if (parentNode != -1) {
              if (is_visited[parentNode])
                parent_already_visited = true;
              else
                is_visited[parentNode] = true;
            }
            //  cost += paidAmount[thisNode];
          }

          // parse through childs of current node
          bool is_counted = false;
          for (long nodes = 0; nodes < num_nodes; nodes++) {
            if (dp_table[thisNode * num_nodes + nodes] == true &&
                is_visited[nodes] == false) {
              is_visited[nodes] = true;
              is_counted = true;
            }
          }

          if (is_counted && !is_investigated) {
            investigatedMembers++;
            cost += paidAmount[thisNode];
          }

          thisNode = parentNode;

          // stop search if all tree is parsed
          if (thisNode < 0 || parent_already_visited)
            break;

          // cout << thisNode << " " << investigatedMembers << endl;
        }
      }

      // check if all nodes are visited
      bool is_all_true = true;
      // cout << "---------------  ";
      for (long nodes = 0; nodes < num_nodes; nodes++) {
        // cout << is_visited[nodes] << " ";
        if (is_visited[nodes] == false) {
          is_all_true = false;
          break;
        }
      }
      //   cout << endl;

      // check is current solution is the better one
      if (is_all_true && (investigatedMembers < minInvestigatedMembers) &&
          (cost > maxCost)) {
        minInvestigatedMembers = investigatedMembers;
        maxCost = cost;
      }
    }

    // printArray(dp_table, num_nodes);
    // printArray(paidAmount, num_nodes);
    cout << minInvestigatedMembers << " " << maxCost << endl;

    // cout << token;
    // cout << num_nodes << endl;
  }

  return 0;
}

// utilty function to print an NxN bool array
void printArray(bool *array, int tam_rows_cols) {
  for (int i = 0; i < tam_rows_cols; i++) {
    for (int j = 0; j < tam_rows_cols; j++) {
      cout << array[i * tam_rows_cols + j] << " ";
    }
    cout << endl;
  }
}

// utilty function to print an N short array
void printArray(short *array, int tam_rows_cols) {
  for (int i = 0; i < tam_rows_cols; i++) {
    cout << array[i] << " ";
  }
  cout << endl;
}

long searchNodeIndex(long *nodeArray, long arraySize, long elem) {
  for (long i = 0; i < arraySize; i++) {
    if (nodeArray[i] == elem)
      return i;
  }
  return -1;
}