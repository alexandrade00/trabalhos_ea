#include <algorithm>
#include <array>
#include <chrono>
#include <cstddef>
#include <cstdlib>
#include <initializer_list>
#include <deque>
#include <iostream>
#include <iterator>
#include <memory>
#include <ostream>
#include <sstream>
#include <string>
#include <set>
#include <unordered_map>
#include <utility>
#include <vector>
#include <map>

using namespace std;

vector<int> getElements(map<int, vector<int>> tree, vector<int> usedNumbers);
void printValues(map<int, vector<int>> tree, vector<int> usedNumbers);
void path(int n);
int best = 0;
vector<deque<int>> treeNumbers;
vector<deque<int>> v;
deque<int> auxFill;

int main(){

    string s;
    int number = 0;
    string auxString;
    vector<int> values;
    int i = 0;
    int j = 0;
    //new map<int, vector<int>> *tree = new map<int, vector<int>>;     
        
        while(std::getline(std::cin,s)){
            
            if(stoi(s) == -1){
                path(i);
                treeNumbers.clear();
                v.clear();
                auxFill.clear();
                i = 0;
            }
            else{
                
                treeNumbers.push_back(std::deque<int>());
                v.push_back(std::deque<int>());
                stringstream ss(s);
                while(ss >> auxString){
                    number = stoi(auxString);

                    if (std::find(auxFill.begin(), auxFill.end(), number) == auxFill.end()){
                        //cout << number << " ";
                        treeNumbers[i].push_back(number);
                        auxFill.push_back(number);
                    }
                    //treeNumbers[i].push_back(number);

                    /*if(number == 0){
                        treeNumbers.push_back(std::deque<int>());
                        v.push_back(std::deque<int>());
                        i++;
                    }*/
                }
                //if(treeNumbers[i].size() > 0)
                //    treeNumbers[i].pop_back();
                
            }
            i++;
        }
        
    return 0;
}

void path(int n){

    for(int i = 0; i < n; i++){
        for (int num: treeNumbers[i]){
            cout << num << " ";
        }
        cout << "\n";
    }/*

    for(int i = 0; i < n; i++){
        for(int j = 0; j <= i; j++){

            if(j > i)
                break;
            
            if(i == 0 && j == 0)
                v[i][j] = treeNumbers[i][j];

            else if(j == i)
                v[i][j] = v[i-1][j-1] + treeNumbers[i][j];
            
            else if(j == 0 && i != 0)
                v[i][j] = v[i-1][j] + treeNumbers[i][j];
            
            else
                v[i][j] = max(v[i-1][j-1], v[i-1][j]) + treeNumbers[i][j];
           // cout << v[i][j] << "\n";
        }
    }
    for(int i = 0; i < n; i++){
        cout << "\n";
        for (int num: treeNumbers[i]){
            cout << num << " ";
        }
    }*/
}

vector<int> getElements(map<int, vector<int>> tree, vector<int> usedNumbers){
    
    int key = 0;
    int temp = 0;
    size_t values = 0;
    size_t prev = 0;
    for (auto x : tree) {
        values = x.second.size() -1;
        if(key == 0){
            //cout << "key: "<< x.first << "  Value size: " << values<< "\n";
            key = 1;
            prev = values;
        }
        else{
            temp = values-prev-1;
            //cout << "key: "<< x.first << "  Value size: " << values - prev - 1<< "\n";
            prev = values; 
        }
        if(temp > 0 && key != 0)
            usedNumbers.push_back(x.first);
    }
    return usedNumbers;
}

void printValues(map<int, vector<int>> tree, vector<int> usedNumbers){
    int total = 0;
    size_t number_of_nodes = usedNumbers.size();
    if(number_of_nodes > 0){
        for(size_t i = 0; i < number_of_nodes; i++){
            int temp = tree.find(usedNumbers[i])->second.back();
            total += temp;
        }
    }
    else
        total = tree.find(0)->second.back();
    cout << number_of_nodes << " " << total << "\n";
    //cout << number_of_nodes << " " << "\n";   
}