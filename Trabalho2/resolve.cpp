#include <algorithm>
#include <cstddef>
#include <iostream>
#include <string>

using namespace std;

void printArray(bool *array, int tam_rows_cols);
void printArray(short *array, int tam_rows_cols);

int main() {
  istreambuf_iterator<char> begin(cin), end;
  string s(begin, end);

  string testcases_delimiter = "-1";
  string newline_delimiter = "\n";

  size_t pos_all = 0;
  size_t pos_one_test_case = 0;

  // delimit inputs by test cases
  string token;
  while ((pos_all = s.find(testcases_delimiter)) != string::npos) {

    int num_nodes = 0;
    token = s.substr(0, pos_all);
    s.erase(0, pos_all + testcases_delimiter.length() + 1);

    // Count newlines, each newline equals to one node
    string aux_string;
    string token_copy = token;
    while ((pos_one_test_case = token_copy.find(newline_delimiter)) !=
           string::npos) {
      aux_string = token_copy.substr(0, pos_one_test_case);
      token_copy.erase(0, pos_one_test_case + newline_delimiter.length());
      num_nodes++;
    }

    // initialize cache and fill with false's
    bool dp_table[num_nodes * num_nodes];
    fill(dp_table, dp_table + num_nodes * num_nodes, false);

    // array with amount paid by each node
    short paidAmount[num_nodes];

    // parse thourgh each line
    short current_node = 0;
    while ((pos_one_test_case = token.find(newline_delimiter)) !=
           string::npos) {
      aux_string = token.substr(0, pos_one_test_case + 1);

      // read each line information and populate data structs
      string concat_nums = "";
      for (long unsigned int i = 0; i < aux_string.length(); i++) {
        if (aux_string.at(i) == ' ') {
          if (stoi(concat_nums) != current_node) {
            dp_table[current_node * num_nodes + stoi(concat_nums)] = true;
            // dp_table[stoi(concat_nums) * num_nodes + current_node] = true;
          }
          concat_nums = "";
        } else if (aux_string.at(i) == '\n') {
          paidAmount[current_node] = stoi(concat_nums);
        } else {
          concat_nums += aux_string.at(i);
        }
      }

      token.erase(0, pos_one_test_case + newline_delimiter.length());
      current_node++;
    }

    // cycle to iterate leaf nodes
    bool is_leaf;
    bool is_visited[num_nodes];
    short investigatedMembers;
    short thisNode;
    short minInvestigatedMembers = 10000;
    int minCost = 100000000;
    int cost = 0;
    for (int i = num_nodes - 1; i >= 0; i--) {
      // at beginning all nodes are not visited
      fill(is_visited, is_visited + num_nodes, false);
      is_visited[0] = true;
      is_leaf = true;
      investigatedMembers = 0;
      cost = 0;

      //check if node is leaf
      for (int nodes = 0; nodes < num_nodes; nodes++) {
        if (dp_table[i * num_nodes + nodes] == true) {
          is_leaf = false;
          break;
        }
      }

      // go to next node if it is not leaf
      if (is_leaf != true)
        continue;

      // starter bottom-up node
      thisNode = i;
      // climb tree up to root node
      while (true) {

        // count investigated nodes and sum cost, leaf node and root does not count
        if (thisNode != i && thisNode != 0) {
          investigatedMembers++;
          is_visited[thisNode] = true;
          cost += paidAmount[thisNode];
        }

        // save last parent node
        short last_node = -1;
        // parse through childs of current node
        for (int nodes = 1; nodes < num_nodes; nodes++) {
          if (dp_table[thisNode * num_nodes + nodes] == true &&
              nodes != thisNode && nodes != thisNode) {
            is_visited[nodes] = true;
            last_node = -1;
            // parse through child nodes of current node childs
            for (int nodes_child = 1; nodes_child < num_nodes; nodes_child++) {
              if (dp_table[nodes * num_nodes + nodes_child] == true &&
                  nodes_child != nodes && is_visited[nodes_child] != true) {
                is_visited[nodes_child] = true;
                // increment variables only one time for each child node 
                if (last_node != nodes) {
                  investigatedMembers++;
                  cost += paidAmount[nodes];
                  last_node = nodes;
                }
              }
            }
          }
        }

        // stop search if all tree is parsed
        if (thisNode == 0)
          break;

        // search for parent of current node
        for (int nodes = 0; nodes < num_nodes; nodes++) {
          if (dp_table[nodes * num_nodes + thisNode] == true) {
            thisNode = nodes;
            break;
          }
        }

        // cout << thisNode << " " << investigatedMembers << endl;
      }

      // check if all nodes are visited
      bool is_all_true = true;
      // cout << "---------------  ";
      for (int nodes = 0; nodes < num_nodes; nodes++) {
        // cout << is_visited[nodes] << " ";
        if (is_visited[nodes] == false) {
          is_all_true = false;
          break;
        }
      }
      // cout << endl;

      // check is current solution is the better one
      if (is_all_true && (investigatedMembers < minInvestigatedMembers) &&
          (cost < minCost)) {
        minInvestigatedMembers = investigatedMembers;
        minCost = cost;
      }
    }

    // printArray(dp_table, num_nodes);
    // printArray(paidAmount, num_nodes);
    cout << minInvestigatedMembers << " " << minCost << endl;

    // cout << token;
    // cout << num_nodes << endl;
  }

  return 0;
}

// utilty function to print an NxN bool array
void printArray(bool *array, int tam_rows_cols) {
  for (int i = 0; i < tam_rows_cols; i++) {
    for (int j = 0; j < tam_rows_cols; j++) {
      cout << array[i * tam_rows_cols + j] << " ";
    }
    cout << endl;
  }
}

// utilty function to print an N short array
void printArray(short *array, int tam_rows_cols) {
  for (int i = 0; i < tam_rows_cols; i++) {
    cout << array[i] << " ";
  }
  cout << endl;
}