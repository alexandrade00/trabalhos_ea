#include <algorithm>
#include <array>
#include <chrono>
#include <cstddef>
#include <cstdlib>
#include <initializer_list>
#include <iostream>
#include <iterator>
#include <memory>
#include <ostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

using namespace std;

struct ArrayHasher {
  std::size_t operator()(const std::array<int, 2> &a) const {
    std::size_t h = 0;

    for (auto e : a) {
      h ^= std::hash<int>{}(e) + 0x9e3779b9 + (h << 6) + (h >> 2);
    }
    return h;
  }
};

bool test_puzzle(int num_pieces_and_tam_board[3]);
bool do_puzzle(int i, int j,
               unordered_map<array<int, 2>, vector<int *>, ArrayHasher> *comb,
               int *board, int *n_pieces_board);
// bool compare_all_sides(int *look_up_piece, int *piece_in_board,
//    short side_to_look);
void rotate_piece(short current_side, short wanted_side, int *piece);
void printBoard(int *board, int n, int m);
void insert_piece_at_board(int *piece, int *board, int x, int y, int num_cols);
bool try_one_piece(int i, int j, int *board, int *n_pieces_board, int *piece);

int main() {
  string inputLine;
  string debug_string;
  int number_test_cases = 0;
  int num_pieces_and_tam_board[3] = {0, 0, 0};

  cin >> number_test_cases;

  for (int j = 0; j < number_test_cases; j++) {

    for (int i = 0; i < 3; i++)
      cin >> num_pieces_and_tam_board[i];
    // auto start = chrono::high_resolution_clock::now();
    test_puzzle(num_pieces_and_tam_board);
    // auto stop = chrono::high_resolution_clock::now();
    // auto duration = chrono::duration_cast<chrono::milliseconds>(stop -start);
    // cout << duration.count() << endl;
  }
}

bool test_puzzle(int num_pieces_and_tam_board[3]) {

  int *all_pieces = new int[(num_pieces_and_tam_board[0] - 1) * 4];
  int *board = new int[num_pieces_and_tam_board[0] * 4];
  int num_of_pieces[1000];
  fill(num_of_pieces, num_of_pieces + 1000, 0);

  unordered_map<array<int, 2>, vector<int *>, ArrayHasher> *comb_new =
      new unordered_map<array<int, 2>, vector<int *>, ArrayHasher>;

  for (int k = 0; k < 4; k++) {
    cin >> *(board + k);
    num_of_pieces[*(board + k)]++;
  }

  for (int i = 0; i < num_pieces_and_tam_board[0] - 1; i++) {

    for (int j = 0; j < 4; j++) {
      cin >> all_pieces[i * 4 + j];
      num_of_pieces[all_pieces[i * 4 + j]]++;
      // cout << all_pieces[i * 4 + j] << " - " << num_of_pieces[all_pieces[i *
      // 4 + j]] << endl;
    }
  }

  int num_odd_pieces = 0;
  for (int i = 0; i < 1000; i++) {
    num_odd_pieces += num_of_pieces[i] % 2 == 0 ? 0 : 1;
  }

  if (num_odd_pieces > 4) {
    // cout << num_odd_pieces << endl;
    cout << "impossible puzzle!" << endl;
    delete[] board;
    delete[] all_pieces;

    return false;
  }

  string combination;
  array<int, 2> int_combination;

  for (int i_piece = 0; i_piece < (num_pieces_and_tam_board[0] - 1) * 4;
       i_piece += 4) {
    for (int k = 0; k < 3; k++) {
      int_combination[0] = *(all_pieces + i_piece + k);
      int_combination[1] = *(all_pieces + i_piece + k + 1);

      if (comb_new->find(int_combination) == comb_new->end()) {
        comb_new->insert(
            pair<array<int, 2>, vector<int *>>(int_combination, {}));
      }

      (*comb_new)[int_combination].push_back((all_pieces + i_piece));
    }

    int_combination[0] = *(all_pieces + i_piece + 3);
    int_combination[1] = *(all_pieces + i_piece);
    if (comb_new->find(int_combination) == comb_new->end()) {
      comb_new->insert(pair<array<int, 2>, vector<int *>>(int_combination, {}));
    }

    (*comb_new)[int_combination].push_back(all_pieces + i_piece);
  }

  // for (auto itr = comb_new->begin(); itr != comb_new->end(); itr++) {
  //   cout << itr->first[0] << " " << itr->first[1] << ": ";
  //   for (auto itr2 = itr->second.begin(); itr2 != itr->second.end(); itr2++)
  //   {
  //     for (int i = 0; i < 4; i++)
  //       cout << *(*(itr2) + i) << " ";
  //     cout << "; ";
  //   }
  //   cout << endl;
  // }

  bool result = do_puzzle(num_pieces_and_tam_board[2] > 1 ? 0 : 1,
                          num_pieces_and_tam_board[2] > 1 ? 1 : 0, comb_new,
                          board, num_pieces_and_tam_board);

  if (result == 1)
    printBoard(board, num_pieces_and_tam_board[1], num_pieces_and_tam_board[2]);
  else {
    cout << "impossible puzzle!" << endl;
    // printBoard(board, num_pieces_and_tam_board[1],
    // num_pieces_and_tam_board[2]);
  }

  delete[] board;
  delete[] all_pieces;

  return false;
}

bool do_puzzle(int i, int j,
               unordered_map<array<int, 2>, vector<int *>, ArrayHasher> *comb,
               int *board, int *n_pieces_board) {

  bool result = false;
  array<int, 4> temp_piece;

  // printBoard(board, n_pieces_board[1], n_pieces_board[2]);
  // cout << "--------------------------------------------------------" << endl;

  if (i < n_pieces_board[1]) {
    if (j < n_pieces_board[2]) {

      if (i == 0) {

        auto itr =
            comb->find({board[i * n_pieces_board[2] * 4 + (j - 1) * 4 + 2],
                        board[i * n_pieces_board[2] * 4 + (j - 1) * 4 + 1]});

        if (itr != comb->end()) {
          for (auto itr_vector = itr->second.begin();
               itr_vector != itr->second.end(); itr_vector++) {

            if (*itr_vector[0] == -1)
              continue;

            array<int, 2> piece_side = {
                board[i * n_pieces_board[2] * 4 + (j - 1) * 4 + 2],
                board[i * n_pieces_board[2] * 4 + (j - 1) * 4 + 1]};
            array<int, 2> temp_side = {-1, -1};
            array<int, 4> look_up_piece = {(*itr_vector)[0], (*itr_vector)[1],
                                           (*itr_vector)[2], (*itr_vector)[3]};

            for (int k = 0; k < 4; k++) {
              temp_side[0] = look_up_piece[k];

              if (k == 3)
                temp_side[1] = look_up_piece[0];
              else
                temp_side[1] = look_up_piece[k + 1];

              if (piece_side == temp_side) {
                for (int m = 0; m < 4; m++)
                  temp_piece[m] = *(*itr_vector + m);
                rotate_piece(k, 3, *itr_vector);

                insert_piece_at_board(*itr_vector, board, i, j,
                                      n_pieces_board[2]);

                if (i == n_pieces_board[1] - 1 && j == n_pieces_board[2] - 1)
                  return true;

                for (int m = 0; m < 4; m++) {
                  *(*itr_vector + m) = -1;
                }

                if (j == n_pieces_board[2] - 1)
                  result = do_puzzle(i + 1, 0, comb, board, n_pieces_board);
                else
                  result = do_puzzle(i, j + 1, comb, board, n_pieces_board);

                if (result == true)
                  return true;

                for (int m = 0; m < 4; m++)
                  *(*itr_vector + m) = temp_piece[m];
              }
            }
          }

          return false;
        }
      } else if (j == 0) {
        auto itr =
            comb->find({board[(i - 1) * n_pieces_board[2] * 4 + j * 4 + 3],
                        board[(i - 1) * n_pieces_board[2] * 4 + j * 4 + 2]});
        if (itr != comb->end()) {
          for (auto itr_vector = itr->second.begin();
               itr_vector != itr->second.end(); itr_vector++) {

            if (*itr_vector[0] == -1)
              continue;

            array<int, 2> piece_side = {
                board[(i - 1) * n_pieces_board[2] * 4 + j * 4 + 3],
                board[(i - 1) * n_pieces_board[2] * 4 + j * 4 + 2]};
            array<int, 2> temp_side = {-1, -1};
            array<int, 4> look_up_piece = {(*itr_vector)[0], (*itr_vector)[1],
                                           (*itr_vector)[2], (*itr_vector)[3]};

            for (int k = 0; k < 4; k++) {
              // cout << k << "--------------\n";
              temp_side[0] = look_up_piece[k];

              if (k == 3)
                temp_side[1] = look_up_piece[0];
              else
                temp_side[1] = look_up_piece[k + 1];

              // copy(look_up_piece.begin(), look_up_piece.end(),
              //       std::ostream_iterator<int>(std::cout, ", "));
              // cout << "\n";
              // copy(piece_side.begin(), piece_side.end(),
              //       std::ostream_iterator<int>(std::cout, ", "));
              // cout << "\n";
              // copy(temp_side.begin(), temp_side.end(),
              //       std::ostream_iterator<int>(std::cout, ", "));
              // cout << "\n\n\n";

              if (piece_side == temp_side) {
                // cout << "entrei" << endl;
                for (int m = 0; m < 4; m++)
                  temp_piece[m] = *(*itr_vector + m);
                rotate_piece(k, 0, *itr_vector);
                // for (int n=0; n<4; n++)
                //   cout << (*itr_vector)[n] << " ";
                // cout << endl;

                insert_piece_at_board(*itr_vector, board, i, j,
                                      n_pieces_board[2]);

                if (i == n_pieces_board[1] - 1 && j == n_pieces_board[2] - 1)
                  return true;

                for (int m = 0; m < 4; m++) {
                  *(*itr_vector + m) = -1;
                }

                if (j == n_pieces_board[2] - 1)
                  result = do_puzzle(i + 1, 0, comb, board, n_pieces_board);
                else
                  result = do_puzzle(i, j + 1, comb, board, n_pieces_board);

                if (result == true)
                  return true;

                for (int m = 0; m < 4; m++)
                  *(*itr_vector + m) = temp_piece[m];
              }
            }
            //   return false;
          }
          return false;
        }
      } else {
        // for (int z = 2; z > 0; z--) {
        //   int pos_1 =0, pos_2 =0;
        //   if (z==2) {
        //     pos_1 = 2;
        //     pos_2=1;
        //   } else {
        //     pos_1 = 1;
        //     pos_2=2;
        //   }

        auto itr =
            comb->find({board[i * n_pieces_board[2] * 4 + (j - 1) * 4 + 2],
                        board[i * n_pieces_board[2] * 4 + (j - 1) * 4 + 1]});

        if (itr != comb->end()) {
          for (auto itr_vector = itr->second.begin();
               itr_vector != itr->second.end(); itr_vector++) {

            if (*itr_vector[0] == -1)
              continue;

            array<int, 2> piece_side_1 = {
                board[i * n_pieces_board[2] * 4 + (j - 1) * 4 + 2],
                board[i * n_pieces_board[2] * 4 + (j - 1) * 4 + 1]};
            array<int, 2> piece_side_2 = {
                board[(i - 1) * n_pieces_board[2] * 4 + j * 4 + 3],
                board[(i - 1) * n_pieces_board[2] * 4 + j * 4 + 2]};
            array<int, 2> temp_side_1 = {-1, -1};
            array<int, 2> temp_side_2 = {-1, -1};
            array<int, 4> look_up_piece = {(*itr_vector)[0], (*itr_vector)[1],
                                           (*itr_vector)[2], (*itr_vector)[3]};

            for (int k = 0; k < 4; k++) {
              temp_side_1[0] = look_up_piece[k];

              if (k == 2) {
                temp_side_1[1] = look_up_piece[k + 1];
                temp_side_2[0] = look_up_piece[k + 1];
                temp_side_2[1] = look_up_piece[0];
              } else if (k == 3) {
                temp_side_1[1] = look_up_piece[0];
                temp_side_2[0] = look_up_piece[0];
                temp_side_2[1] = look_up_piece[1];
              } else {
                temp_side_1[1] = look_up_piece[k + 1];
                temp_side_2[0] = look_up_piece[k + 1];
                temp_side_2[1] = look_up_piece[k + 2];
              }

              // copy(look_up_piece.begin(), look_up_piece.end(),
              //      std::ostream_iterator<int>(std::cout, ", "));
              // cout << "\n";
              // copy(piece_side_1.begin(), piece_side_1.end(),
              //      std::ostream_iterator<int>(std::cout, ", "));
              // cout << "\n";
              // copy(temp_side_1.begin(), temp_side_1.end(),
              //      std::ostream_iterator<int>(std::cout, ", "));
              // cout << "\n\n";
              // copy(piece_side_2.begin(), piece_side_2.end(),
              //      std::ostream_iterator<int>(std::cout, ", "));
              // cout << "\n";
              // copy(temp_side_2.begin(), temp_side_2.end(),
              //      std::ostream_iterator<int>(std::cout, ", "));
              // cout << "\n\n\n";

              if (piece_side_1 == temp_side_1 && piece_side_2 == temp_side_2) {

                for (int m = 0; m < 4; m++)
                  temp_piece[m] = *(*itr_vector + m);
                // cout << "entrei"
                //      << "\n\n";
                rotate_piece(k, 3, *itr_vector);
                insert_piece_at_board(*itr_vector, board, i, j,
                                      n_pieces_board[2]);

                if (i == n_pieces_board[1] - 1 && j == n_pieces_board[2] - 1)
                  return true;

                for (int m = 0; m < 4; m++) {
                  *(*itr_vector + m) = -1;
                }

                if (j == n_pieces_board[2] - 1)
                  result = do_puzzle(i + 1, 0, comb, board, n_pieces_board);
                else
                  result = do_puzzle(i, j + 1, comb, board, n_pieces_board);

                if (result == true)
                  return true;

                for (int m = 0; m < 4; m++)
                  *(*itr_vector + m) = temp_piece[m];
              }
            }
          }
        }
      }
    }
  }
  return false;
}

void rotate_piece(short current_side, short wanted_side, int *piece) {

  short number_of_rotations = wanted_side - current_side;
  if (number_of_rotations < 0)
    number_of_rotations += 4;

  for (int i = 0; i < number_of_rotations; i++) {
    int last_elem = piece[3];
    for (int j = 2; j >= 0; j--)
      piece[j + 1] = piece[j];
    piece[0] = last_elem;
  }
}

void printBoard(int *board, int n, int m) {
  for (int i = 0; i < n; i++) {
    if (i != 0)
      cout << "\n";

    for (int j = 0; j < m; j++) {
      cout << *(board + i * m * 4 + j * 4 + 0) << " "
           << *(board + i * m * 4 + j * 4 + 1);
      if (j != m - 1)
        cout << "  ";
    }

    cout << "\n";
    for (int j = 0; j < m; j++) {
      cout << *(board + i * m * 4 + j * 4 + 3) << " "
           << *(board + i * m * 4 + j * 4 + 2);
      if (j != m - 1)
        cout << "  ";
    }
    cout << "\n";
  }
}

void insert_piece_at_board(int *piece, int *board, int x, int y, int num_cols) {

  for (int i = 0; i < 4; i++)
    board[x * num_cols * 4 + y * 4 + i] = piece[i];
}
