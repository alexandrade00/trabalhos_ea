import random as r

def main():
    number_of_rows: int = 2500
    number_of_columns: int = 1
    number_of_pieces: int = number_of_rows * number_of_columns
    
    list_of_pieces = []
    unique_pieces = []

    while (len(unique_pieces) < number_of_pieces):
        list_of_pieces.append(list(map(str, r.sample(range(80), 4))))
        unique_pieces = [list(x) for x in set(tuple(x) for x in list_of_pieces)]
    
    
    number_of_pieces: int = len(unique_pieces)

    print(f"{number_of_pieces} - {number_of_rows * number_of_columns}")

    out_file = open("generated_inputs.txt", mode='w', encoding='utf-8')
    out_file.write(f"1\n{number_of_pieces} {number_of_rows} {number_of_columns}\n")
    for i in range(number_of_pieces):
        out_file.write(f'{" ".join(unique_pieces[i])}\n')
    out_file.close()


if __name__ == "__main__":
    main()