#include <algorithm>
#include <iostream>
#include <numeric>
#include <queue>

using namespace std;

class sort_indices {
private:
  int *mparr;

public:
  sort_indices(int *parr) : mparr(parr) {}
  bool operator()(int i, int j) const { return mparr[i] < mparr[j]; }
};

void findOrder(bool *adj, int *inDegree, int *timeAmount, int numOperations,
               int statistic);
int biggestTime(bool *adj, int *sumTime, int node, int numOperations);

bool isBottleneck(bool *isRunning, int numOperations);

int main() {
  int numOperations = 0;
  int tempNode = 0;
  int statistic = -1;
  cin >> numOperations;

  int inDegree[numOperations];
  fill(inDegree, inDegree + numOperations, -1);

  int runTime[numOperations];
  fill(runTime, runTime + numOperations, -1);

  bool adj[numOperations * numOperations];
  fill(adj, adj + numOperations * numOperations, false);

  bool isTerminal[numOperations];
  fill(isTerminal, isTerminal + numOperations, true);

  for (int i = 0; i < numOperations; i++) {
    cin >> runTime[i] >> inDegree[i];

    if (inDegree[i] > 0) {
      while (cin.peek() != '\n') {
        cin >> tempNode;
        adj[(tempNode - 1) * numOperations + i] = true;
        isTerminal[tempNode - 1] = false;
      }
    }
  }
  cin >> statistic;

  //   for (int i = 0; i < numOperations; i++) {
  //     for (int j = 0; j < numOperations; j++)
  //       cout << adj[i * numOperations + j] << " ";
  //     cout << endl;
  //   }

  //   for (int i = 0; i < numOperations; i++)
  //     cout << inDegree[i] << " ";
  //   cout << endl;

  int countTerminals = 0;
  for (int i = 0; i < numOperations; i++)
    if (isTerminal[i])
      if (++countTerminals > 1) {
        cout << "INVALID" << endl;
        return 0;
      }

  findOrder(adj, inDegree, runTime, numOperations, statistic);

  return 0;
}

void findOrder(bool *adj, int *inDegree, int *timeAmount, int numOperations,
               int statistic) {

  priority_queue<int, vector<int>, greater<int>> pq;
  vector<int> bottleNeckNodes;
  bool isRunning[numOperations];
  fill(isRunning, isRunning + numOperations, false);
  int sumTime[numOperations];
  fill(sumTime, sumTime + numOperations, 0);
  int totalTime = 0;
  int currentNode = 0;

  bool zeroFound = false;
  for (int i = 0; i < numOperations; i++) {
    if (inDegree[i] == 0) {
      if (!zeroFound) {
        pq.push(i);
        isRunning[i] = true;
        totalTime += timeAmount[i];
        zeroFound = true;
        sumTime[i] = timeAmount[i];
      } else {
        cout << "INVALID" << endl;
        return;
      }
    }
  }

  int *order = new int[numOperations];
  int orderIndex = 0;
  for (int i = 0; i < numOperations; i++) {

    if (pq.empty()) {
      cout << "INVALID" << endl;
      return;
    }

    currentNode = pq.top();
    pq.pop();

    order[orderIndex++] = currentNode;

    if (statistic == 2) {
      int time = biggestTime(adj, sumTime, currentNode, numOperations);
      sumTime[currentNode] = time + timeAmount[currentNode];
    }

    for (int j = 0; j < numOperations; j++) {
      if (adj[j * numOperations + currentNode])
        isRunning[j] = false;
    }

    if (statistic == 3 && isBottleneck(isRunning, numOperations))
      bottleNeckNodes.push_back(currentNode);

    for (int j = 0; j < numOperations; j++) {
      if (adj[currentNode * numOperations + j] == true) {
        if (--inDegree[j] == 0) {
          pq.push(j);

          isRunning[j] = true;

          if (statistic == 1)
            totalTime += timeAmount[j];
        }
      }
    }
  }

  switch (statistic) {

  case 0:
    cout << "VALID" << endl;
    break;

  case 1:
    cout << totalTime << endl;
    for (int i = 0; i < numOperations; i++)
      cout << order[i] + 1 << endl;
    break;

  case 2:
    cout << sumTime[currentNode] << endl;
    break;

  case 3:
    for (auto i : bottleNeckNodes)
      cout << i + 1 << endl;
    break;
  }

  delete[] order;
}

int biggestTime(bool *adj, int *sumTime, int node, int numOperations) {

  int maxTime = 0;
  for (int rows = 0; rows < numOperations; rows++) {
    if (adj[rows * numOperations + node] == true && sumTime[rows] > maxTime)
      maxTime = sumTime[rows];
  }

  return maxTime;
}

bool isBottleneck(bool *isRunning, int numOperations) {
  bool oneTrue = false;
  for (int i = 0; i < numOperations; i++) {
    if (isRunning[i] == true) {
      if (oneTrue)
        return false;
      else
        oneTrue = true;
    }
  }
  return oneTrue;
}
