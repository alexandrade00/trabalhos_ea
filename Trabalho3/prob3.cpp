#include <algorithm>
#include <ctype.h>
#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>
using namespace std;
vector<vector<int>> elemInfo;

int main() {
  int opNumber;
  int statistic;
  int Check_if_invalid(vector<vector<int>> graph);
  int Check_if_final_invalid(vector<vector<int>> graph);
  int findCycles(vector<vector<int>> graph);
  void case1(vector<vector<int>> graph);
  void case2(vector<vector<int>> graph);
  void case3(vector<vector<int>> graph);
  string str;

  while (std::cin >> opNumber) {
    for (int i = 0; i <= opNumber; i++) {
      std::getline(std::cin, str);
      elemInfo.push_back(std::vector<int>());
      stringstream ss(str);
      string word;
      while (ss >> word)
        elemInfo[i].push_back(stoi(word));
    }
    
    cin >> statistic;
    int flag = 0;
    // Se algum destes condicoes for verdadeiro entao o programa e invalido
    flag += Check_if_invalid(elemInfo);
    flag += Check_if_final_invalid(elemInfo);
    flag += findCycles(elemInfo);
    if (flag > 0)
      cout << "INVALID";
    else {
      if (statistic == 0)
        cout << "VALID";
      else if (statistic == 1)
        case1(elemInfo);
      else if (statistic == 2)
        case2(elemInfo);
      else if (statistic == 3)
        case3(elemInfo);
    }
  }
}

int Check_if_invalid(vector<vector<int>> graph) {
  int k = 0;
  // Se existir mais do que um estado inicial a pipeline e invalida
  for (size_t i = 0; i < graph.size(); i++) {
    if (graph[i].size() < 3)
      k++;
  }
  if (k > 2)
    return 1;

  else
    return 0;
}

int Check_if_final_invalid(vector<vector<int>> graph) {
  // Actually isto esta errado, estou a assumir que o problema ocorrera apenas
  // no fim da pipeline
  int size = graph.size() - 1;
  int size1 = graph.size() - 2;
  if (graph[size].size() == graph[size1].size())
    return 1;
  else
    return 0;
}

int findCycles(vector<vector<int>> graph) {
  // Tb trollei, acho que esta verificacao esta incorreta
  // A instrucao final pode ter mais do que uma dependencia, algo que eu nao
  // estou a considerar
  for (size_t i = 0; i < graph.size(); i++) {
    if (graph[i].size() > 3) {
      for (size_t j = 0; i < graph[j].size(); j++) {
        if (j > 2) {
          int instructionToGo = graph[i][j];
          if (graph[instructionToGo].size() > 3) {
            cout << "INVALID";
            return 1;
          }
        }
      }
    }
  }
  return 0;
}

void case1(vector<vector<int>> graph) {
  int sum = 0;
  int array[graph.size() - 1][2];

  // Preenche um array auxiliar com as operacoes e o tempo que estas demoram a
  // executar
  for (size_t i = 0; i < graph.size(); i++) {
    for (size_t j = 0; j < graph[i].size(); j++) {
      if (j == 0)
        array[i][j] = i;
      else if (j == 1) {
        array[i][j] = graph[i][0];
        // cout << array[i][j] << " ";
      } else
        array[i][j] = 0;
    }
  }
  for (size_t i = 1; i < graph.size(); i++) {
    for (size_t j = 0; j < 2; j++) {
      if (j == 1)
        sum += array[i][j];
    }
  }
  cout << sum << "\n";

  size_t k = 0;
  while (k != graph.size() -
                  1) { // Percorre o array todo e verifica a ordem das operacoes

    int bestI = 0;
    int temp = 100000;

    for (size_t i = 0; i < graph.size(); i++) {
      if (array[i][0] != 0 && array[i][0] < temp) {
        temp = array[i][0];
        bestI = i;
      }
    }
    cout << bestI + 1 << "\n";
    array[bestI][0] = 0;
    k++;
  }
}
void case2(vector<vector<int>> graph) {
  // Preenche um array auxiliar com as operacoes e com o ultimo elemento do qual
  // depende Devia preencher com todos os elementos do qual depende
  int array[graph.size() - 1][2];
  int sum = 0;
  for (size_t i = 0; i < graph.size(); i++) {
    for (size_t j = 0; j < graph[i].size(); j++) {
      if (j == 0)
        array[i][j] = graph[i][0];
      else if (j == 1)
        array[i][j] = graph[i][graph[i].size() - 1];
      else
        array[i][j] = 0;
    }
  }
  // Percorre o array todo e verifica que operacoes usar
  for (size_t i = 1; i < graph.size(); i++) {
    int temp = array[i][1];
    int temp1 = 0;
    if (array[i][1] != -1)
      temp1 = array[i][0];

    if (array[i][1] != -1) {
      for (size_t j = i + 1; j < graph.size(); j++) {
        if (array[j][1] == temp) {
          array[j][1] = -1;
          if (array[j][0] >= temp1) {
            temp1 = array[j][0];
          }
        }
      }
    }
    // cout << temp1 << "\n";
    sum += temp1;
  }
  cout << sum << "\n";
}

void case3(vector<vector<int>> graph) {

  vector<int> bottlenecks;
  for (size_t i = 1; i < graph.size(); i++) {

    if (graph[i].size() == 2) // Se for o caso inicial trata se de um bottleneck
      bottlenecks.push_back(i);
    else {
      int auxValues[graph[i].size() - 2]; // array apenas com as dependencias
      // cout << graph[i].size()-2 << "\n";
      int k = 0;

      for (size_t j = 2; j < graph[i].size(); j++) { // Prenche o array auxiliar
        auxValues[k] = graph[i][j];
        k++;
        //  cout << graph[i][j]<< " ";
      }
      // cout << "\n";

      for (size_t j = i + 1; j < graph.size();
           j++) { // Comparacoes com array auxiliar
        // cout << graph[j].size() - 2 << " " << graph[i].size() - 2 << "\n";
        if (graph[j].size() - 2 ==
            graph[i].size() - 2) { // apenas compara com array do mesmo tamanho
          size_t equal = 0;
          for (size_t k = 0; j < graph[i].size();
               k++) { // Se houverem diferencas incrementa variavel eq
            if (auxValues[k] == graph[j][k + 2])
              equal++;
          }
          cout << j << "\n";
          if (equal ==
              graph[i].size() - 2) // se for igual considera se um bottleneck
            bottlenecks.push_back(i);
        }
      }
    }
  }

  // for(size_t i = 0; i < bottlenecks.size(); i++)
  //     cout << bottlenecks[i] << "\n";
}